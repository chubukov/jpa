# README #

### What is jPlateAnalyzer ###

This is a basic package for processing time course data from multiwell plate readers. It is primarily intended for microbial growth curves, but could be applied to other types of data.

Since some of the major functionality is plotting the data, the packages is best used with the [ipython](http://ipython.org) notebook.

Current version is 0.15.10.18 (October 18, 2015)

### How do I get set up? ###

The easiest way to install is with pip

`pip install git+https://bitbucket.org/chubukov/jpa`

Requirements (should be automatically installed by pip):  

  * numpy
  * matplotlib
  * seaborn
  * pandas
  * xlrd

Both python3 and python2 are supported, though testing has primarily been done on python3.4.

### Tutorials ###

Look in the examples directory for some sample ipython notebooks and data sets that you can play with.

### Who do I talk to? ###

If you have suggestions, create an issue from the menu on the left, or talk to Victor Chubukov