from __future__ import unicode_literals
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


import sys

def u(x):
    """convert a string to unicode appropriately for either python 2 or python 3"""
    if sys.version_info < (3,):
        return unicode(x)
    else:
        return str(x)

def float_or_nan(x):
    try:
        return float(x)
    except ValueError:
        return np.nan

def od_transform_plastic_cover(x):
    """best fit for TecanF200 OD reading with breathable sealable cover."""
    return np.polyval([ 0.08888929,  1.28721646,  1.87130784, -0.14837213],x)

def od_transform_standard_lid(x):
    """best fit for TecanF200 OD reading with standard plate lid."""
    return np.polyval([1.4,-0.7,2.95,-0.26],x)


def fit_xy(df_x,df_y,deg=2,t_lim=None,x_lim=None,y_lim=None,plot=False,fittype='poly'):
    """
    This function calculates the fit of dataframes x and y. Use this to

    Parameters:
    df_x: pandas dataframe
        Time course dataframe; for autofluor fit use OD data, for example J.OD590
    df_y: pandas dataframe
        Time course dataframe; for autofluor fit use FP data, for example J.GFP
    t_lim: tuple or list
        The time index for which to do the fit, for example t_lim=(0,4) fits first 4 hours
    x_lim: tuple or list
        The df_x range over which to do the fit, for example x_lim=(0,2.0)
    y_lim: tuple or list
        The df_y range over which to do the fit, for example y_lim=(0,30)
    plot: boolean
        Default is False. True will provide a visual for the data and fit.
    fittype: string
        only 'poly' supported for now. 'exp' or others may be added.
    degree: int
        degree of polynomial to fit. default = 2. Later may choose this automatically

    Returns:
    tuple(f,p)
    f: function providing the best fit of y=f(x)
    p: best fit parameters

    Example to fit autofluorescence:
    d = J.well_info.query('strain=="BW25113"')
    af_GFP,p_GFP=fit_xy(J.OD590[d.well],J.GFP[d.well],deg=3,t_lim=(0,4.84),plot=True)
    J.add_tc(J.GFP-J.OD590.applymap(af_GFP),'GFPnormalized')
    J.GFPnormalized
    """

    df_fit=pd.DataFrame(data={'X':df_x.unstack(),'Y':df_y.unstack()})
    df_fit=df_fit.reset_index([0,1],drop=False)
    df_fit.columns=['well','time','X','Y']
    df_fit=df_fit.sort_values(by='X') #for plotting the line
    if t_lim is not None:
        df_fit=df_fit[(df_fit['time']>=t_lim[0]) & (df_fit['time']<=t_lim[1])]
    if x_lim is not None:
        df_fit=df_fit[(df_fit['X']>=x_lim[0]) & (df_fit['X']<=x_lim[1])]
    if y_lim is not None:
        df_fit=df_fit[(df_fit['Y']>=y_lim[0]) & (df_fit['Y']<=y_lim[1])]

    if fittype=='poly':
        p_fit=np.polyfit(df_fit.X,df_fit.Y,deg=deg)
        # need to change to proper error throwing.

    #elif fittype=='exp'
        #p_fit=scipy.curvefit(..
        #not implementing for now
    else:
        print("only fittype='poly' supported for now")
        return

    fit_func = lambda x: np.polyval(p_fit,x)

    if plot:
        plt.plot(df_fit.X,df_fit.Y,'o')
        plt.plot(df_fit.X,df_fit.X.apply(fit_func))


    return fit_func,p_fit

